package cn.oesoft.blog.service;

import java.util.List;

import cn.oesoft.blog.model.ResponseData.StaticticsBo;
import cn.oesoft.blog.model.domain.Article;
import cn.oesoft.blog.model.domain.Comment;

//博客站点统计服务
public interface ISiteService {

  // 最新收到的评论
  public List<Comment> recentComments(int count);

  // 最新发表的文章
  public List<Article> recentArticles(int count);

  // 获取后台统计数据
  public StaticticsBo getStatistics();

  // 更新某个文章的统计数据
  public void updateStatistics(Article article);

}
