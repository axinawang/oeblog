package cn.oesoft.blog.service;

import java.util.List;

import com.github.pagehelper.PageInfo;

import cn.oesoft.blog.model.domain.Article;

public interface IArticleService {
    // 分页查询文章列表,第一页page=1
    public PageInfo<Article> selectArticleWithPage(Integer page, Integer count);
    // 统计前10的热度文章信息
    public List<Article> getHeatArticles();
}
